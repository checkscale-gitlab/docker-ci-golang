# Docker CI Golang

My personal Golang CI Docker image for Gitlab.

Based on golang:1.11-alpine adding

- git
- openssh
- make
- gcc
- goreleaser
